function customDefault(testObject, propObject) {

    if (typeof testObject == "object" && typeof testObject != null) {
        for (key in propObject) {
            if (testObject[key] == undefined) {
                testObject[key] = propObject[key];
            }
        }
        return testObject;
    }
    else {
        return testObject;
    }

}
module.exports = customDefault;
