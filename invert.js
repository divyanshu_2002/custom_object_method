function customInvert(object) {
    if (typeof object == "object" && typeof object != null) {
        let newObject = {};

        //Method-1
        // console.log(object);
        // for(key in object){
        //     newObject[object[key]]=key;
        // }
        // return newObject;



        //Method-2
        const keys = Object.keys(object);


        for (let index = 0; index < keys.length; index++) {
            const keyData = keys[index];
            newObject[object[keyData]] = keyData;
        }

        return newObject;
    }
}

module.exports = customInvert;