function customKey(testObject) {
    if (typeof testObject == "object" && typeof testObject != null) {
        const keyArray = [];
        for (key in testObject) {
            keyArray.push(key);
        }
        return keyArray;
    }
    else {
        return [];
    }
}

module.exports = customKey;