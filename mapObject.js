function customMapObject(testObject, callBack) {
    if (typeof testObject == "object" && typeof testObject != null) {
        for (key in testObject) {
            callBack(key, testObject[key], testObject);
        }
        return testObject;
    }
    else {
        return {};
    }
}

module.exports = customMapObject;