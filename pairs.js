function customPairs(testObject) {
    if (typeof testObject == "object" && typeof testObject != null) {
        let pairArray = [];
        for (key in testObject) {
            const keyData = key;
            const valueData = testObject[key];
            const tempArray = [keyData, valueData];
            // console.log(tempArray);
            pairArray.push(tempArray);
        }
        return pairArray;
    }
}

module.exports = customPairs;