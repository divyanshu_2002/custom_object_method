const testObject = { name: 'Bruce Wayne' };
const propObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


const customDefault = require("../defaults");

try {
    testObject = customDefault(testObject, propObject);
    console.log(`The object after the default method is`);
    console.log(testObject);
}
catch (error) {
    console.log(error.message);
}
