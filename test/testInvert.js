const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };


const customInvert = require("../invert");

try {
    const returnedObject = customInvert(testObject);

    if (returnedObject) {
        console.log(`The object returned from the testObject method is`);
        console.log(returnedObject);
    }
    else {
        console.log("No object is returned");
    }
}
catch (error) {
    console.log(error.message);
}