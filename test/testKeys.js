const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const customKey = require("../keys");

try {
    const keyArray = customKey(testObject);

    if (keyArray.length != 0) {
        console.log(`The array of keys are `);
        console.log(keyArray);
    }
    else {
        console.log(`The returned array is containing no elements`);
    }

}
catch (error) {
    console.log(error.message);

}



