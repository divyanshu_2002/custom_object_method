const customMapObject = require("../mapObject");

const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

try {
    let modifiedObject = customMapObject(testObject, (key, value, object) => {
        if (typeof value == "number") {
            value = value * 2;
            object[key] = value;
        }
        if (typeof value == "string") {
            value = value.toUpperCase();
            object[key] = value;
        }

    });
    if (modifiedObject) {
        console.log(`The modified object are `);
        console.log(modifiedObject);
    }
    else {
        console.log("Empty Object is returned");
    }
}
catch (error) {
    console.log(error.message);
}








