const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const customPairs = require("../pairs");

try {
    const returnedPair = customPairs(testObject);
    if (returnedPair) {
        console.log(`The returned pairs are`);
        console.log(returnedPair);
    }
    else {
        console.log(`The returned array is of length 0 , please pass a object with keys and values to get the pair`);
    }
}
catch (array) {
    console.log(array);
}