const testObject = { name: 'Bruce Wayne', age: 36, location: 'Gotham' };

const customValues = require("../values");

try {
    const valuesArray = customValues(testObject);

    if (valuesArray.length != 0) {
        console.log(`The array returned by custom values function is`);
        console.log(valuesArray);
    }
    else {
        console.log(`The array returned by custom_values method has no elements`);
    }

}
catch (error) {
    console.log(error.message);
}
