function customValues(testObject) {
    if (typeof testObject == "object" && typeof testObject != null) {
        const valuesArray = [];
        for (key in testObject) {
            valuesArray.push(testObject[key]);
        }
        return valuesArray;
    }
    else {
        return [];
    }
}

module.exports = customValues;